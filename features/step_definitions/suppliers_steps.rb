Dado("que esteja logado e na pagina de suppliers com") do |table|
    @supplier = Suppliers.new
    @login2 = Login.new
    @home = Home.new

    @usuario = table.rows_hash

    @home.visita
    #@login2.login('admin@phptravels.com','demoadmin')
    @login2.login(@usuario[:email], @usuario[:senha])
    @supplier.acessa_add_suppliers
end

Quando("faço o cadastro de um supplier") do
    @supplier.faz_cadastro
end

Então("vejo o a mensagem {string}") do |mensagem|
 expect(@supplier.valida_supplier).to eql mensagem
end