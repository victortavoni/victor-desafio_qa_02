Dado("que eu acessei a pagina de login") do                                   
    @home = Home.new
    @home.visita
  end                                                                           
                                                                                
  Quando("faço login com") do |table|                                           
    @usuario = table.rows_hash
    @login = Login.new
    @login.login(@usuario[:email], @usuario[:senha])
     
  end                                                                           
                                                                                
  Então("sou autenticado com sucesso") do                                       
   expect(@login.verifica_login).to eql "Super Admin"
  end                                                                           