require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'


Capybara.configure do |c|
    c.default_driver = :selenium_chrome
    c.app_host = 'https://www.phptravels.net/admin'
    c.default_max_wait_time = 50
end