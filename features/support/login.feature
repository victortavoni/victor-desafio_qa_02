#language: pt

 Funcionalidade: Login

Para que eu possa cadastrar um SUPPLIER
Sendo um usuario cadastrado
Posso acessor o sistema com meu email e sennha

Cenario: Login usuario

    Dado que eu acessei a pagina de login
    Quando faço login com
    | email | admin@phptravels.com |
    | senha | demoadmin            |
    Então sou autenticado com sucesso
