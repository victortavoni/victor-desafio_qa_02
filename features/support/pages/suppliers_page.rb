require 'faker'
require 'rspec'

class Suppliers
    include Capybara::DSL
    
    
    
    def acessa_add_suppliers
        visit '/accounts/suppliers/add'

    end

    def faz_cadastro

        @name = Faker::Name.first_name
        @lastName = Faker::Name.middle_name
        @email = Faker::Internet.email(@name)
        @cellphone = Faker::PhoneNumber.phone_number
        @address1 = Faker::Address.street_address
        @address2 = Faker::Address.secondary_address
        @companyName = Faker::Company.name

       find('input[name= fname]').set @name
       find('input[name= lname]').set @lastName
	   find('input[name= email]').set @email
       find('input[name= password]').set '123456'
       find('input[name= mobile]').set '00000000000'
       find(:select, 'country').first(:option, 'Guadaloupe').select_option
       find('input[name= address1]').set @address1
       find('input[name= address2]').set @address2
       find('input[name= itemname]').set @companyName
       find(:select, 'hotels[]').first(:option, 'Hyatt Regency Perth').select_option
       find(:select, 'tours[]').first(:option, "Spectaculars Of The Nile 3 Nights").select_option
       find(:select, 'cars[]').first(:option, 'Kia Pacanto 2014').select_option 

       click_button "Submit"
   
    end

    def valida_supplier
        within('.ui-pnotify ') do
            return find('.ui-pnotify-title').text
        end
        
    end
end