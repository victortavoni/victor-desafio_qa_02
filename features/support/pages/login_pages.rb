
class Login
    include Capybara:: DSL

    def login (email, senha)
        find("input[placeholder=Email]").set email
        find("input[name=password]").set senha
        click_button "Login"
        
    end

    def verifica_login
        return find('.user').text
    end
end
