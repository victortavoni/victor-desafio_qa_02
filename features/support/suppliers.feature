#language : pt

Funcionalidade: Suppliers

    Sendo um adminsitrador
    Posso cadastrar um suppliers
    Para que meu site tenha mais um suppliers

    @smoke
        Cenario: Cadastro de suppliers
        
            Dado que esteja logado e na pagina de suppliers com
            | email | admin@phptravels.com |
            | senha | demoadmin |
            Quando faço o cadastro de um supplier
            Então vejo o a mensagem "CHANGES SAVED!"